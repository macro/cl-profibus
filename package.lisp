;;;; package.lisp

(defpackage #:cl-profibus
  (:use #:cl))

(defpackage #:gsd-parser
  (:use #:cl #:cl-ppcre)
  (:nicknames #:parser)
  (:export "get-global-config"))
