;;;; cl-profibus.asd

(asdf:defsystem #:cl-profibus
  :description "communicate with profibus-dp"
  :author "Zhang Hai Ming"
  :license  "MIT License"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "cl-profibus")
	       (:file "gsd-parser")))
